class AddTotalVotesToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :total_votes, :integer, default: 0, limit: 1
  end
end
