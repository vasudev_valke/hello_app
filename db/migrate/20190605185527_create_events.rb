class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.integer :micropost_id
      t.integer :category_id, limit: 1
      t.string :category_name

      t.timestamps
    end
  end
end
