# HEALTHIFY
by Vasudev Valke

Description:
A web application built on Ruby on Rails where you can post a message through Whatsapp and view the message trends in the dashboard.

To Create your own application:

Clone the application:
SSH- git clone git@bitbucket.org:vasudev_valke/hello_app.git
HTTPS- git clone https://vasudev_valke@bitbucket.org/vasudev_valke/hello_app.git

To Set the environment variables:
Update below variables in /env.yml file.

AUTHY_API_KEY: '<AUTHY_API_KEY>' 
TWILIO_ACCOUNT_SID: '<TWILIO_ACCOUNT_SID>' 
TWILIO_AUTH_TOKEN: '<TWILIO_AUTH_TOKEN>' 
TWILIO_NUMBER: '<TWILIO NUMBER>'
GMAIL_USERNAME: '<YOUR_GMAIL_USERNAME>' 
GMAIL_PASSWORD: '<YOUR_GMAIL_PASSWORD>' 
SENDGRID_USERNAME: '<SENDGRID_USERNAME>' 
SENDGRID_PASSWORD: '<SENDGRID_PASSWORD>' 
CRYPT_PASSWORD: '<RANDOM_KEY_TO_ENCRYPT_MESSAGE>' 
MAIL_HOST: '<ngrok_URL>'
SECRET_KEY_BASE: 'DEV SECRET KEY BASE'
TWILIO_SMS_NUMBER: '<TWILIO PHONE NUMBER>'
SECRET_KEY_TEST: 'TEST SECRET KEY BASE'
SECRET_KEY_PROD: 'PROD SECRET KEY BASE'

Login to [*TWILIO*](https://www.twilio.com/login):
You will find the AUTHY_API_KEY in twilio app. This is required to receive OTP to your mobile number during login.
Also TWILIO_ACCOUNT_SID and TWILIO_AUTH_TOKEN is required to send/receive whatsapp message.

Run the application: rails server -b $IP -p 3000

[*Healthify*](https://<random_string>.ngrok.io) Link is currently inactive. 
NGROK: is used to expose local server over internet. This is required to send/receive whatsapp messages via public URL. I am using free trial ngrok account. In the free trial account, Link will be inactive once the session ends. Everytime you want to start a new session, ngrok will generate a new link to expose your local server over internet.

Download [*ngrok*](https://ngrok.com/download) and follow the steps provided in the same link. 

Copy the forwarding URL https://<random_string>.ngrok.io and paste it in your browser.
To send and receive messages from the twilio whatsapp Sandbox to your Application, you need to save the ngrok URL- https://<random_string>.ngrok.io/ in the twilio sandbox webhook field.

Save the Twilio WhatsApp Number: +14155238886. You need to first send "join attempt-round" to the twilio number through whatsapp to activate your number to send/receive whatsapp messages from twilio. Once twilio confirms on registration of your number, you can Send your message and view it in your application dashboard.