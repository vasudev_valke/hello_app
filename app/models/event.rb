class Event < ApplicationRecord
	belongs_to :micropost
	validates :user_id, presence: true
	enum category_id: {
		high_risk: 0,
		mother: 1,
		child: 2,
		referral: 3,
		delivery: 4,
		stable: 5,
		alert: 6,
		anemia: 7,
		malnutrition: 8,
		uncoded: 9
	}
	KEY_WORDS = {
		'high risk': 0,
		'mother': 1,
		'child': 2,
		'refer': 3,
		'treatment': 3,
		'delivery': 4,
		'normal': 5,
		'teek': 5,
		'alert': 6,
		'hb': 7,
		'iron': 7,
		'muac': 8
	}
	MUAC_VALUE = 12.5
def self.set_events_for_a_micropost micropost_id: nil
		micropost = Micropost.find(micropost_id)
		user = micropost.user_id
		micropost_data = micropost.content.downcase
		micropost_data_words = micropost_data.split(' ')
		KEY_WORDS.each do |key_word,key_word_type|
			if micropost_data_words.include? key_word.to_s
				  if key_word.to_s == 'muac'
				    	micropost_in_digits = []
				    	micropost_data.split('').each do |ch|
					    	if ch == '.'
					    		micropost_in_digits.push ch
					    	else  if ch[/\d+/].nil?
					     			    micropost_in_digits.push ' '
					    	    	else
					    		    	micropost_in_digits.push ch[/\d+/]
					    	    	end
					    	end
				    	end
				  	if micropost_in_digits.join.split(" ").map{|x| x.to_i < MUAC_VALUE}.include? true
					    	Event.create(micropost_id: micropost.id, category_name: key_word, category_id: key_word_type, user_id: user)	
				  	end
		  		else
		    			Event.create(micropost_id: micropost.id, category_name: key_word, category_id: key_word_type, user_id: user)
		  		end
			elsif key_word.to_s =='high risk' and micropost_data.include? 'high risk'
				Event.create(micropost_id: micropost.id, category_id: 0, category_name: key_word, user_id: user)
			end
		end
		    Event.create(micropost_id: micropost.id, category_id: 9, category_name: "uncoded", user_id: user) if micropost.events.count.zero? 
	end
end