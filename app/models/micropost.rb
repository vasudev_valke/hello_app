class Micropost < ApplicationRecord
    belongs_to :user
    default_scope -> { order(created_at: :desc) }
    validates :user_id, presence: true
    validates :content, presence: true, length: { maximum: 140 }
    has_many :events, dependent: :destroy
    
      def self.to_csv
        user_attributes = %w{name phone_number}
        category_attribute = %w{category_id}
        micropost_attribute = %w{created_at}
        message_attribute = %w{content}
        password = ENV['CRYPT_PASSWORD']
        micropost_attribute = %w{created_at id}
        if Rails.env.development?
          a = 0
        else
          a = 616
        end
          message_attribute = %w{content}
          password = ENV['CRYPT_PASSWORD']
        CSV.generate(headers: true) do |csv|
            csv << user_attributes +  micropost_attribute + category_attribute + message_attribute
          all.each do |micropost|
              data = []
              data = data.push(AESCrypt.decrypt(micropost.content, password))
            csv <<  micropost.user.attributes.values_at(*user_attributes) + micropost.attributes.values_at(*micropost_attribute) + micropost.events.pluck(:category_id) + data
          end
        end
      end
      
      def set_events  
	    Event.set_events_for_a_micropost micropost_id: self.id
	end
end
