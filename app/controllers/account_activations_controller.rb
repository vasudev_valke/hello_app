class AccountActivationsController < ApplicationController

def edit
    user = User.find_by(email: params[:email])
    if user
      if !user.activated?
        if user.authenticated?(:activation, params[:id])
          user.update_attribute(:activated,    true)
          user.update_attribute(:activated_at, Time.zone.now)
      #log_in user
          flash[:success] = "Your Email ID is activated! Please Login..."
          redirect_to new_session_path
        else
          flash[:danger] = "Invalid authentication failed"
          redirect_to root_url
        end
      else
        flash[:danger] = "User already activated, try logging in"
        redirect_to root_url
      end
    else
      flash[:danger] = "Invalid user"
      redirect_to root_url
    end
  end
end