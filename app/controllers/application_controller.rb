class ApplicationController < ActionController::Base
protect_from_forgery prepend: true, with: :exception
include SessionsHelper
  
 private

    # Confirms a logged-in user.
    def logged_in_user
      unless signed_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to new_session_path
      end
    end

protected

  def authenticate!
    redirect_to new_session_path and return unless signed_in?
  end
  
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end