class UsersController < ApplicationController
before_action :authenticate! ,  only: :show
before_action :logged_in_user , only: [:index, :edit, :update, :destroy, :change_role_to_admin, :change_role_to_worker, :delete_an_worker, :show]
before_action :correct_user,   only: [:edit, :update]
before_action :admin_user,     only: [:destroy, :index, :change_role_to_admin, :change_role_to_worker, :delete_an_worker]
before_action :send_otp, only: :edit
require 'will_paginate/array'
  # shows all users 
  def index
    @users = User.all.order(admin: :DESC)
    @users = @users.paginate(:page => params[:page], :per_page => 8)
  end
  
  # shows user profile
  def show
      @user = User.find(params[:id])
      @microposts = @user.microposts.paginate(:page => params[:page], :per_page => 5)
  end

  # create new user
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.account_activation(@user).deliver_now
      authy = Authy::API.register_user(
      email: @user.email,
      cellphone: @user.phone_number,
      country_code: @user.country_code )
      @user.update(authy_id: authy.id)
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  # update password
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    token = Authy::API.verify(id: @user.authy_id, token: params[:token])
    if token.ok?
    if @user.authenticate(params[:user][:current_password]) && @user.update_attributes(user_params)
      flash[:success] = "Password updated"
      redirect_to @user
      else
      flash.now[:danger] = "Invalid current password"
      render 'edit'
    end
  else
    flash.now[:danger] = "Invalid token"
      render 'edit'   
  end
end

  # delete user
  def destroy
   User.find(params[:id]).destroy
   flash[:success] = "Profile deleted"
   redirect_to users_path
  end

def send_otp
    if @user.id == session[:user_id]  
    Authy::API.request_sms(id: @user.authy_id)
    flash.now[:info] = "OTP has been sent to your phone. It is not set to default"
end
end

  def change_role_to_admin
    user = User.find(params['id'])
    user.total_votes = 0
    user.update_attribute(:admin, true)
    flash[:success] = "Promoted as admin"
    redirect_back(fallback_location: users_path)
  end
  
  def delete_an_worker
    user = User.find(params['id'])
    user.destroy
    flash[:success] = "Profile deleted"
    redirect_to users_path
  end
  
  def change_role_to_worker
    user = User.find(params['id'])
    if current_user.following?(user)
    flash[:info] = "Your Vote is deleted"
    current_user.unfollow(user)
    user.update_attribute(:total_votes, user.followers.count)
    else
    flash[:info] = "Your Vote is registered to remove admin "
    current_user.follow(user)
    user.update_attribute(:total_votes, user.followers.count)
    end
    if user.total_votes >= (0.75 * (User.where(admin: true).count))
      flash[:success] = "Removed admin"
      user.total_votes = 0
      user.update_attribute(:admin, false)
      redirect_to delete_an_worker_user_path
    else
      redirect_back(fallback_location: users_path)
    end
  end
  
  
  
 private
 def user_params
    params.require(:user).permit(:id, :name, :email, :current_password, :password, 
                                   :password_confirmation, :phone_number, :country_code, :admin)
  end
  
  def logged_in_user
    unless signed_in?
    flash[:danger] = "Please log in."
    redirect_to new_session_path
    end
  end
  
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_path) unless current_user?(@user)
  end
  
  def admin_user
      redirect_to(root_path) unless current_user.admin?
  end

end