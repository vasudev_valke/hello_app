class RelationshipsController < ApplicationController
#before_action :find_user

  def follow
    user = User.find(params['id'])
    current_user.follow(user)
    redirect_to users_path
end


  def unfollow
    user = User.find(params['id'])
    current_user.unfollow(user)
    redirect_to users_path
end

def find_user
  @user = User.find(params[:user_id])
end
def logged_in_user
    unless signed_in?
    store_location
    flash[:danger] = "Please log in."
    redirect_to new_session_path
    end
  end
  
end
