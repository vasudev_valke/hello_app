class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:index, :create, :show, :destroy]
  before_action :correct_user,   only: :destroy
  
  # logs page
  def index
  if current_user.admin?
    @microposts = Micropost.all
    if params[:user_id].present?
      user_id = params[:user_id]
    @microposts = Micropost.where(:user_id => user_id)
    end
  else
    @microposts = current_user.microposts.all
  end
     if params[:category_id].present?
        array = []
        @microposts.each do |micropost|
          array.push micropost.id if micropost.events.pluck(:category_id).join(',').include? params[:category_id]
        end
        @microposts = Micropost.where(id: array)
     end
     if params[:start_date].present? && params[:end_date].present?
       start_date = Date.parse(params[:start_date]).to_s
       end_date = ((Date.parse(params[:end_date])).to_time+1.day).to_s
       if start_date <= Date.today.to_s
          if start_date <= end_date
            @microposts = @microposts.where(:created_at => start_date..end_date)
            #@microposts = @microposts.where('created_at < ?', end_date)
          else
            flash.now[:danger] = "Start date should not be greater than End date"
          end
       else
          flash.now[:danger] = "Start date should not be greater than current date"
       end
        else if params[:start_date].present?
            start_date = Date.parse(params[:start_date]).to_s
            if start_date <= Date.today.to_s
              @microposts = @microposts.where('created_at > ?', start_date)
            else 
              flash.now[:danger] = "Start date should not be greater than todays date"
            end
         else if params[:end_date].present?
            end_date = ((Date.parse(params[:end_date])).to_time+1.day).to_s
            @microposts = @microposts.where('created_at < ?', end_date)
         end
        end
      end
      if current_user.admin?
      respond_to do |format|
        if params[:format] == "csv"
          format.csv { send_data @microposts.to_csv }
        else
          request.format = "html"
          format.html
          end
      end
    else
      respond_to do |format|
        if params[:format] == "csv"
          @microposts = current_user.microposts.all
          format.csv { send_data @microposts.to_csv }
        else
          request.format = "html"
          format.html
          end
      end
    end
   end
    
   # create a micropost 
  def create
    @user = current_user
    @micropost = current_user.microposts.build(micropost_params)
    
    require 'despamilator'
    if @micropost.save
      @micropost.set_events
      if Despamilator.new(@micropost.content).score > 0.2
        @micropost.update_attribute(:spam, 'true')
      end
      password = ENV['CRYPT_PASSWORD']
      data = @micropost.content
      if Rails.env.development?
        content = AESCrypt.encrypt(data, password)
      else
        content = data
      end
      @micropost.update_attribute(:content, content)
      @event = Event.find_by(micropost_id: @micropost.id)
      ist_time = ((@micropost.created_at).to_time+5.hours+30.minutes).to_datetime
      @micropost.update_attribute(:created_at, ist_time)
      @event.update_attribute(:created_at, ist_time)      
      #flash[:success] = "Micropost created!"
      #redirect_to user_path(current_user)
      respond_to do |format|
      format.js
      end
    else
      flash[:info] = "User sent a blank message"
      redirect_to user_path(current_user)
    end
 end

 # delete micropost
  def destroy
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    #respond_to do |format|
     # format.js
    redirect_to request.referrer || user_path(current_user)
  #end
end
  
  # Summary page which shows chart and events table
  def show
  if current_user.admin?
    @events = Event.all
  else
    @events = Event.all
    @events = @events.where(:user_id => current_user.id)
  end
    if params[:user_id].present?
      user_id = params[:user_id]
      @events = @events.where(:user_id => user_id)
    end
    if params[:category_id].present?
      category_id = params[:category_id]
      @events = @events.where(:category_id => category_id)
    end
  if params[:start_date].present? && params[:end_date].present?
       start_date = Date.parse(params[:start_date]).to_s
       end_date = ((Date.parse(params[:end_date])).to_time+1.day).to_s
       if start_date <= Date.today.to_s
          if start_date <= end_date
            @events = @events.where(:created_at => start_date..end_date)
            #@events = @events.where('created_at < ?', end_date)
          else
            flash.now[:danger] = "Start date should not be greater than End date"
          end
       else
          flash.now[:danger] = "Start date should not be greater than today"
       end
        else if params[:start_date].present?
            start_date = Date.parse(params[:start_date]).to_s
            if start_date <= Date.today.to_s
              @events = @events.where('created_at > ?', start_date)
            else 
              flash.now[:danger] = "Start date should not be greater than today"
            end
         else if params[:end_date].present?
            end_date = ((Date.parse(params[:end_date])).to_time+1.day).to_s
              @events = @events.where('created_at < ?', end_date)
         end
        end
      end
end
  private

    def micropost_params
      params.require(:micropost).permit(:content, :user_id)
    end
    
    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end

end