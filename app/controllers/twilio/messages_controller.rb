class Twilio::MessagesController < ApplicationController

#To post a message in application sent via whatsapp

skip_before_action :verify_authenticity_token
  def create
    require 'despamilator'
      #@user = User.where(phone_number: params['From'].split('+91').second).first
      @user = User.find(29)
      if @user.present?
        @micropost = Micropost.new(user_id: @user.id, content: params['Body'])
          if @micropost.save
            @micropost.set_events
            if Despamilator.new(@micropost.content).score > 0.2
              @micropost.update_attribute(:spam, 'true')
            end
            #password = ENV['CRYPT_PASSWORD']
            data = @micropost.content
            #content = AESCrypt.encrypt(data, password)
            @micropost.update_attribute(:content, data)
            @event = Event.find_by(micropost_id: @micropost.id)
            ist_time = ((@micropost.created_at).to_time+5.hours+30.minutes).to_datetime
            @micropost.update_attribute(:created_at, ist_time)
            @event.update_attribute(:created_at, ist_time)      
          end
      else
        message = "Please register with Healthify app, before sending a message. 
        https://bitbucket.org/vasudev_valke/hello_app/src/master/README.md "
        response = Twilio::TwiML::MessagingResponse.new
        response.message(body: message)
        render xml: response.to_xml
      end  
  end
end