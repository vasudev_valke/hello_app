class SessionsController < ApplicationController
 
  def new
    @user = User.new
  end

  def create
    @user = User.find_by(email: params[:email])
    if @user 
      if @user.authenticate(params[:password])
          if @user.activated?
          session[:pre_2fa_auth_user_id] = @user.id
          #Authy::API.request_sms(id: @user.authy_id)
          redirect_to sessions_two_factor_path
        else
          flash.now[:danger] = "Email ID is not activated, Please check your mail to activate account!"
          @user ||= User.new(email: params[:email])
          render :new
        end
      else
        @user ||= User.new(email: params[:email])
        flash.now[:danger] = "Invalid Username/Password! Please enter the right credentials."
        render :new
      end
    else
      @user ||= User.new(email: params[:email])
      flash[:danger] = "Email ID is not registered, Please SignUp."
      redirect_to new_user_path
    end
  end
  
   def two_factor
       @user = User.find(session[:pre_2fa_auth_user_id])
   end
   
   def authy_verify
    @user = User.find(session[:pre_2fa_auth_user_id])
    token = Authy::API.verify(id: @user.authy_id, token: params[:token])
    if token.ok?
      flash[:danger] = "Incorrect code, Please click on resend code"
      redirect_to sessions_two_factor_path
    else
      session[:pre_2fa_auth_user_id] = nil
      session[:user_id] = @user.id
      @user = User.find(session[:user_id])
      #flash[:success] = "Welcome to Healthify App"
      redirect_to user_path(User.find(session[:user_id]))
    end
  end
  
  def authy_send_token
    @user = User.find(session[:pre_2fa_auth_user_id])
    #Authy::API.request_sms(id: @user.authy_id)
    flash[:info] = "Code has been re-sent"
    redirect_to sessions_two_factor_path
  end

  def destroy
    session[:user_id] = nil
    flash[:info] = "You have been logged out"
    redirect_to new_session_path
  end
end