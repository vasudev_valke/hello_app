class StaticPagesController < ApplicationController
  
  def home
    if signed_in?
      @user = User.find(session[:user_id])
      @micropost = @user.microposts.build
  end
end
def profile
  @user = User.find(params[:id])
end
end
