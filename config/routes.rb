Rails.application.routes.draw do

  resources :admin, only: [:edit, :update]
  get '/signup', to: 'users#new'
  get "sessions/two_factor"
  post "sessions/authy_verify"
  post "sessions/authy_send_token"

  namespace :twilio do
    post 'messages', to: 'messages#create'
  end

  resources :sessions, only: [:new, :create]
  delete "sessions/destroy"
  root 'static_pages#home'
  
  resources :users, only: [:new, :create, :edit, :destroy]
  resources :users, only: [:index, :update, :show,] do
    member do
      get 'change_role_to_admin'
      get 'change_role_to_worker'
      get 'delete_an_worker'
	  end
  end
  namespace :twilio do
    post 'messages', to: 'messages#create'
  end
  resources :users do
    member do
      get :following, :followers
    end
  end
  
  resources :relationships, only:[:create, :destroy] do
      member do
        post 'follow'
        delete 'unfollow'
      end
  end
  
  resources :account_activations, only: [:edit]
  #resources :whatsapp_messages, only: [:create, :show]  
  resources :microposts, only: [:index, :create, :destroy, :show] do
    member do
      post 'collect_ids'
      
      get 'collect_ids'
    end
  end

resource :microposts do
  collection do
    post 'create'
  end
end
end