require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "login with invalid user" do
    get new_session_path
    assert_template 'sessions/new'
    post sessions_path, params: { session: { email: "", password: "" } }
    assert_not flash.empty?
    get new_user_path
  end
  
  test "login with wrong password" do
    get new_session_path
    assert_template 'sessions/new'
    post sessions_path, params: { session: { email: @user.email, password: "" } }
    assert_not flash.empty?
    get new_session_path
  end
  
  def setup
    @user = users(:michael)
  end
end
