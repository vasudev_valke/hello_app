require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    #assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              email: @user.email,
                                              current_password: "password",
                                              password:              "foo",
                                              password_confirmation: "bar",
                                              token: "123"} }

    get edit_user_path(@user)
  end
test "successful edit" do
  log_in_as(@user)
    get edit_user_path(@user)
    #assert_template 'users/edit'
    name = @user.name
    email = @user.email
    patch user_path(@user), params: { user: { name:  name,
                                              email: email,
                                              password:              "passwords",
                                              password_confirmation: "passwords" } }
    #assert_redirected_to @user
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end
  end