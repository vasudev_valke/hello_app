require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @third_user = users(:lana)
  @fourth_user = users(:malory)
  @fifth_user = users(:truly)
  @sixth_user = users(:advan)
  end
  
   test "should redirect index when not logged in" do
    get new_session_path
    assert_template 'sessions/new'
  end

test "should get new" do
    get root_path
    assert_response :success
  end

test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to new_session_path
  end

  test "should redirect update when not logged in" do
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to new_session_path
  end
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to new_session_path
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to new_session_path
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
    get delete_an_worker_user_path(@other_user)
  end
    assert_redirected_to new_session_path
  end
test "should direct destroy when logged in" do
    log_in_as(@user)
    get delete_an_worker_user_path(@other_user)
    assert_difference 'User.count', -1 do
    @other_user.destroy
    end
    log_in_as(@user)
    assert_response :success
  end

  test "should redirect destroy when logged in as a non-admin" do
  log_in_as(@other_user)
    !@other_user.admin?
    assert_no_difference 'User.count' do
    get delete_an_worker_user_path(@user)
  end
    assert_redirected_to new_session_path
  end
  
  test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch user_path(@other_user), params: {
                                    user: { password:              "password",
                                            password_confirmation: "password",
                                            admin: true } }
    assert_not @other_user.admin?
  end
  test "should redirect following when logged in as non admin" do
    log_in_as(@other_user)
    get change_role_to_worker_user_path(@user)
    assert_redirected_to new_session_path
  end
  test "should redirect following when not logged in" do
    get change_role_to_worker_user_path(@user)
    assert_redirected_to new_session_path
  end

 test "Vote or Unvote to remove as admin" do
    log_in_as(@user) 
    get change_role_to_worker_user_path(@other_user)
    assert_not @user.following?(@other_user)
    assert_difference '@user.following.count', 1 do
      @user.follow(@other_user)
    end
    assert @user.following?(@other_user)
    assert_difference '@user.following.count', -1 do
      @user.unfollow(@other_user)
    end
  end

 test "should redirect delete when count is greater 75% of admin" do
    log_in_as(@fourth_user) 
    assert_difference '@user.followers.count', 1 do
      @fourth_user.follow(@user)
    end
    @user.update_attribute(:admin, false)
    assert_difference 'User.count', -1 do
    @user.destroy
  end
  end

  test "should redirect delete when deleting non admin" do
    log_in_as(@user)
    assert_difference 'User.count', -1 do
    @sixth_user.destroy
    end    
  end
end